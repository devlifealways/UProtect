package PEngine;

public interface ICrypto {

	public String privateKey();

	public String publicKey();

}
