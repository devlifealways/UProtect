package PNetworking;


import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.URL;
import java.util.ResourceBundle;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class FxmlControllerServer extends Application implements Initializable{ 
	
	final int WITH = 650;
	final int HEIGHT = 500;
    Server server;
	final String MyName="Bob";
	Image _off_notification;
	
	
	
	@FXML
	private ImageView imageView;
	
	@FXML
	private Button btnSend;
	
	@FXML 
	private TextField editMessage;
	
	@FXML 
	public ListView<String> listView;
	
	
	 @Override
	 public void initialize(URL url, ResourceBundle rb) {

			try {	
				
			   _off_notification= new Image(getClass().getResource("notification_off.png").toString());	
				imageView.setImage(_off_notification);			
				btnSend.setDefaultButton(true);
						
				@SuppressWarnings("resource")
				ServerSocket  serverSocket=new ServerSocket(Server.PORT,100,InetAddress.getByName("0.0.0.0"));
				server=new Server(serverSocket.accept(),listView,imageView);
				server.start();
				
			} catch(Exception e) {
				System.err.println("start : ");
				e.printStackTrace();
			}

	    }
	
	
	 @FXML 
	 void handleSubmitMessage(ActionEvent event){
	
		String value = editMessage.getText();
		
		if(value.length()!=0){
	
			try {

				
				listView.getItems().add("Moi> "+value);
				editMessage.setText("");
				server.sendMessage(MyName+"> "+value);
				


			} catch (Exception e) {
			
				System.err.println("handleSubmitMessage:");
				e.printStackTrace();
			}
		}
		
	}
	 
	 @FXML
	void updateNotification(MouseEvent event){
			imageView.setImage(_off_notification);			

	}
	
	
	
	
	@Override
	public void start(Stage primaryStage) {
		try {
			
			Parent root = FXMLLoader.load(getClass().getResource("fxml_server.fxml"));
			Scene scene = new Scene(root,WITH,HEIGHT);
			primaryStage.setTitle("UProtect");
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			primaryStage.show();
						
			
	       
			
		} catch(Exception e) {
			System.err.println("start : ");
			e.printStackTrace();
		}
	}
	
	
	
	
	
	public static void main(String[] args) {
		
		System.out.println("Server Starting... ");
		ConfigNetwork config=new ConfigNetwork();
		config.RouterGivenAdress();
    
        
        
		launch(args);
			
	}
	
	
	

}
